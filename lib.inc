%define SYS_READ 0
%define SYS_WRITE 1
%define SYS_EXIT 60

%define IO_STDIN 0
%define IO_STDOUT 1
%define IO_STDERR 2

%define SPACE 0x20
%define ENTER 0xA
%define TAB 0x9


section .text

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, SYS_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.loop:
    cmp byte[rdi+rax], 0
    je .return
    inc rax
    jmp .loop
.return:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, SYS_WRITE
    mov rdi, IO_STDOUT
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, ENTER

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rax, SYS_WRITE
    mov rdx, 1
    mov rdi, IO_STDOUT
    syscall
    pop rdi
    ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    test rdi, rdi
    jns print_uint
    neg rdi
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
   

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	mov rax, rdi
	mov r8, rsp
	push 0
	mov r9, 10 
	.loop:
		xor rdx, rdx
		div r9
		add rdx, '0'
		dec rsp
		mov byte[rsp], dl
		test rax, rax			
		jnz .loop	
	mov rdi, rsp
	push r8
	call print_string
	pop rsp
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
  	.loop:
  		xor rdx, rdx			
  		mov dl, byte[rsi+rax]
        cmp dl, [rdi+rax]
  		jne .er
  		test dl, dl
  		je .point			
  		inc rax
  		jmp .loop	
  	.er:
  		xor rax, rax
  		ret
  	.point:
  		mov rax, 1	
  		ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	push 0
    mov rax, SYS_READ
    mov rdi, IO_STDIN
    mov rsi, rsp
    mov rdx, 1
	syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	push r12
	push r13
	push r14
    mov r12, rdi
    mov r13, rsi
    xor r14, r14
    .read_char:
    	call read_char
    	test rax, rax
 	    je .point
 		cmp r14, r13
 		jge .er
    	cmp rax, SPACE
    	je .skip_first
    	cmp rax, TAB
 		je .skip_first
 		cmp rax, '\n'
 		je .skip_first	
 		mov [r12+r14], rax
 		inc r14
 		jmp .read_char
 	.skip_first:
 		test r14, r14
 		je .read_char
 	.point:
 		mov byte [r12+r14+1], 0	
 		mov rax, r12
 		mov rdx, r14
 		jmp .allret
 	.er:
 		xor rax, rax
 		xor rdx, rdx
 	.allret:
 		pop r14
 		pop r13
 		pop r12
 		ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx
    xor r8,  r8
    mov r9,  10
.loop:
    mov cl,  [rdi+r8]
    cmp cl,  '0'
    jl  .return
    cmp cl,  '9'
    jg  .return
    sub cl,  '0'
    mul r9
    add rax, rcx
    inc r8
    jmp .loop
.return:
    mov rdx, r8
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte [rdi], '-'
    je .neggative
    jmp parse_uint
	.neggative:
		inc rdi
		call parse_uint
		cmp rdx, 0
		je .fail
		inc rdx
		neg rax
		ret
	.fail:
		xor rax, rax
		ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    .loop:
        cmp rax, rdx
        jge .fail
        mov r11b, byte [rdi + rax]
        mov byte [rsi + rax], r11b
        test r11b, r11b
        jz .success
        inc rax
        jmp .loop
    .fail:
        xor rax, rax
    .success:
    ret
